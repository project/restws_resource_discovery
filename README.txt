Allows a list of resource endpoints to be viewed at /restws_resource.json.

Motivation: a 3rd party system may not be aware of all entity types available, so a list of entity types is useful. There are some obvious entity types, such as node, comment etc but any custom entity types will not be known to a 3rd party.

Method: A new entity type is created - "restws_resource". The module automatically creates an entity for every restws resource.

There is no configuration for this module, the /restws_resource.json endpoint will automatically become available.
